package steps;
import io.qameta.allure.Step;
import page.LoginPage;
import service.ConfProperties;

import static com.codeborne.selenide.Selenide.*;

public class LoginSteps {

    @Step("Заполнение поля 'Логин'")
    public void fillLogin(String login) {
        $(new LoginPage().onLoginForm().loginField).setValue(login);
    }

    @Step("Заполнение поля 'Пароль'")
    public void fillPassword(String password) {
        $(new LoginPage().onLoginForm().passwordField).setValue(password);
    }

    @Step("Клик по кнопке 'Войти'")
    public void clickLoginButton() {
        $(new LoginPage().onLoginForm().loginButton).pressEnter();
    }

    @Step("Открыть стенд")
    public void openTest() {
        open(ConfProperties.getProperty("web.baseUrl"));
    }
}