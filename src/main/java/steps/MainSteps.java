package steps;

import io.qameta.allure.Step;
import page.MainPage;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class MainSteps {

    @Step("Проверка видимости текста на главной странице")
    public void shouldBeExist() {
        $(new MainPage().onMainForm().myStatus).shouldBe(visible);
    }
}