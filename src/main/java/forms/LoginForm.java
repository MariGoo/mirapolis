package forms;

import org.openqa.selenium.By;

public class LoginForm {
    public By loginButton = By.id("button_submit_login_form");
    public By loginField = By.name("user");
    public By passwordField = By.name("password");
}