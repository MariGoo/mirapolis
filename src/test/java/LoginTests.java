import com.codeborne.selenide.Selenide;
import io.qameta.allure.Step;
import io.qameta.allure.Story;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import service.ConfProperties;
import steps.LoginSteps;
import steps.MainSteps;

public class LoginTests {

    private LoginSteps loginSteps = new LoginSteps();
    private MainSteps mainSteps = new MainSteps();

    @Test (priority = 2)
    @Story(value = "Успешная регистрация на сайте")
    public void checkLoginPass() {
        loginSteps.openTest();
        loginSteps.fillLogin(ConfProperties.getProperty("web.login"));
        loginSteps.fillPassword(ConfProperties.getProperty("web.password"));
        loginSteps.clickLoginButton();
        mainSteps.shouldBeExist();
    }

    @DataProvider
    public Object[][] loginData() {
        return new Object[][]{
                {ConfProperties.getProperty("web.login"), ""},
                {"", ConfProperties.getProperty("web.password")},
                {"", ""}
        };
    }

    @Test(dataProvider = "loginData", priority = 1)
    @Story(value = "Регистрация на сайте с ошибкой")
    @Step("пароль = '{login}', логин = '{password}'")
    public void checkLoginError(String login, String password) {
        loginSteps.openTest();
        loginSteps.fillLogin(login);
        loginSteps.fillPassword(password);
        loginSteps.clickLoginButton();
        Selenide.switchTo().alert().accept();
    }
}